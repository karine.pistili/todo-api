# To Do API

A simple API created with Nodejs and Express. Database connection with mysql.

## Install dependencies

```
npm install
```

## Run

```
node index.js
```