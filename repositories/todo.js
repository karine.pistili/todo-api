const TodoSchema = require('../database/schemas/todo')
const db = require('../database/config')

const table = TodoSchema.tableName
const fields = TodoSchema.fields

class TodoRepository {
    static async list() {
        const query = `select * from ${TodoSchema.tableName}`
        return await db.queryValues(query)
    }

    static async create(values) {
        const query = `insert into ${TodoSchema.tableName} (${fields}) values (${values})`
        return await db.queryValues(query)
    }

    static async get(id) {
        const condition = `where id = ${id}`
        const query = `select * from ${TodoSchema.tableName} ${condition}`
        return await db.queryValues(query)
    }

    static async update(id, values) {
        const condition = `where id = ${id}`
        const query = `update ${TodoSchema.tableName} set ${values} ${condition}`
        return await db.queryValues(query)
    }

    static async delete(id) {
        const condition = `where id = ${id}`
        const query = `delete from ${TodoSchema.tableName} ${condition}`
        return await db.queryValues(query)
    }
}

module.exports = TodoRepository