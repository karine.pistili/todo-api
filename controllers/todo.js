const TodoRepository = require('../repositories/todo')
const TodoModel = require('../models/Todo')
const TodoSchema = require('../database/schemas/todo')

module.exports = {
    list: async () => {
        try {
            const data = await TodoRepository.list()
            const parsed = []

            data.forEach(todo => {
                parsed.push(new TodoModel(
                    todo.id,
                    todo.title,
                    todo.description,
                    todo.date
                ))
            })

            return { status: 200, data: parsed }
        }
        catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    create: async data => {
        try {
            const todo = new TodoSchema(null, data.title, data.description, data.date)
            await TodoRepository.create(todo.toInsertDbValues())
            return { status: 201, data: 'Success creating' }
        } catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    get: async id => {
        try {
            const todo = (await TodoRepository.get(id))[0]
            if (!todo)
                return { status: 404, data: 'Not found' }

            const parsed = new TodoModel(
                todo.id,
                todo.title,
                todo.description,
                todo.date
            )

            return { status: 200, data: parsed }
        }
        catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    update: async data => {
        try {
            const todo = new TodoSchema(data.id, data.title, data.description, data.date)
            await TodoRepository.update(todo.id, todo.toUpdateDbValues())
            return { status: 200, data: 'Success updating' }
        } catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    },
    delete: async id => {
        try {
            await TodoRepository.delete(id)
            return { status: 200, data: 'Success deleting' }
        } catch (e) {
            console.log(e);
            return { status: 500, data: e }
        }
    }
}