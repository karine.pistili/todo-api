const express = require("express")
const router = express.Router()
const todoRoutes = require("./routes/todo")

router.use('/todo', todoRoutes)

module.exports = router